#!/usr/bin/python

ANSIBLE_METADATA = {
    'metadata_version': '1.1',
    'status': ['preview'],
    'supported_by': 'community'
}

DOCUMENTATION = '''
Depends on `helm` command being available on the host it is executed on.
To run on the host where the playbook was executed, use delegate_to e.g.
```
  roles:
    - role: hypi-charts
      delegate_to: 127.0.0.1
```
It assumes kubectl is configured to point to the correct environment so before running, set this using e.g.
```
kubectl config use-context staging
```
'''

EXAMPLES = '''
- name: Install Rook Ceph Operator
  helm_shell:
    namespace: default
    name: rook-ceph
    version: 0.8.0
    source:
      type: directory
      location: "{{ role_path }}/files/platform/rook" # could also use some lookup mechanism "{{lookup('<some-lookup-plugin>', '<path-to-chart>')}}"
'''

RETURN = '''
'''

import os.path
import os
from ansible.module_utils.basic import AnsibleModule
from pkg_resources import parse_version
import collections


def output_val(v):
    if isinstance(v, list):
        return False, '{' + ','.join([output_val(o)[1] for o in v]) + '}'
    elif isinstance(v, bool):
        # helm docs says to use --set-string for bools because it casts to int but that appears to be folly
        return False, str(v).lower()
    elif isinstance(v, str):
        return True, helm_quote(v)
    else:
        return False, v  # '"' + str(v) + '"'


def output_key(k):
    return helm_quote(k)


def helm_quote(k):
    quote = False
    # escaping doesn't seem to quite work as described in the docs
    if '.' in k:
        k = k.replace('.', r'\.')
        quote = True
    if ',' in k:
        k = k.replace(',', r'\,')
        quote = True
    return '"' + k + '"' if quote else k


def flatten(d, parent_key='', sep='.'):
    items = []
    for k, v in d.items():
        k = output_key(k)
        new_key = parent_key + sep + k if parent_key else k
        if isinstance(v, collections.MutableMapping):
            items = items + flatten(v, new_key, sep=sep)
        else:
            is_str, v = output_val(v)
            if is_str:
                items.append('--set-string %s=%s' % (new_key, v))
            else:
                items.append('--set %s=%s' % (new_key, v))
    return items


def run_module():
    # TODO add support for global helm args --debug, --home, --host, --kube-context, --tiller-namespace
    # TODO add support for install --verify, --replace and expand check_mode support to use helm's --dry-run
    # TODO add support for upgrade --verify, --force --recreate-pods and expand check_mode support to use helm's --dry-run
    module_args = dict(
        name=dict(type='str', required=True),
        version=dict(type='str', required=False),
        source=dict(type='dict', required=True),
        namespace=dict(type='str', required=False),
        state=dict(type='str', required=False, default='present'),
        set=dict(type='dict', required=False, default={}),
        opts=dict(type='dict', required=False, default={}),
        values=dict(type='str', required=False),
    )

    # seed the result dict in the object
    # we primarily care about changed and state
    # change is if this module effectively modified the target
    # state will include any data that you want your module to pass back
    # for consumption, for example, in a subsequent task
    result = dict(
        changed=False,
        original_message='',
        message=''
    )

    # the AnsibleModule object will be our abstraction working with Ansible
    # this includes instantiation, a couple of common attr would be the
    # args/params passed to the execution, as well as if the module
    # supports check mode
    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    kubeconf = os.environ.get('KUBECONFIG')
    kubectx = os.environ.get('KUBECTX')

    command = "helm"
    if kubeconf:
        command = "helm --kubeconfig %s %s" % (kubeconf, kubectx)
    if kubectx:
        command = "kubectl config use-context %s && %s" % (kubectx, kubeconf)
    # if the user is working with this module in only check mode we do not
    # want to make any changes to the environment, just return the current
    # state with no modifications
    if module.check_mode:
        return result

    chart_namespace = module.params['namespace']
    if chart_namespace:
        chart_namespace = "--namespace='%s'" % chart_namespace
    else:
        chart_namespace = ''
    chart_state = module.params['state']
    chart_name = module.params['name']
    chart_version = parse_version(module.params['version'])
    chart_location = module.params['source']['location']
    chart_values = module.params['values']
    if chart_values:
        chart_values = '--values=%s' % chart_values
    else:
        chart_values = ''
    # ['--set ' + str(k) + '=' + str(v) for k, v in module.params['set']['install'].items() if 'install' in module.params['set']]
    helm_set_install_args = ''
    # todo add support for list params https://docs.helm.sh/using_helm/#the-format-and-limitations-of-set
    if 'install' in module.params['set']:
        helm_set_install_args = ' '.join([v for v in flatten(module.params['set']['install'])])
    helm_set_upgrade_args = ''
    if 'upgrade' in module.params['set']:
        helm_set_upgrade_args = ' '.join([v for v in flatten(module.params['set']['upgrade'])])

    # (rc, out, err) = module.run_command("pwd && ls", use_unsafe_shell=True)
    # return module.fail_json(msg=module.jsonify([rc, out, err]))
    if chart_state == 'absent':
        cmd_str = "%s delete %s '%s'" % (command, chart_namespace, chart_name)
        (rc, out, err) = module.run_command(cmd_str, use_unsafe_shell=False)
        if rc:
            return module.fail_json(msg=err, rc=rc, cmd=cmd_str)
        result['changed'] = True
        result['message'] = 'Deleted chart %s' % chart_name
        result['original_message'] = out
        return module.exit_json(**result)

    req_file = os.path.join(chart_location, 'requirements.yaml')
    if os.path.isfile(req_file) \
            and 'run_dep_update' in module.params['opts'] and module.params['opts']['run_dep_update']:
        cmd_str = "%s dependency update %s" % (command, chart_location)
        (rc, out, err) = module.run_command(cmd_str, use_unsafe_shell=False)
        if rc:
            return module.fail_json(msg=err, rc=rc, cmd=cmd_str)

    cmd_str = "%s ls %s --all | grep '%s' | cut -f 4,5 | xargs" % (command, chart_namespace, chart_name)
    (rc, out, err) = module.run_command(cmd_str, use_unsafe_shell=True)
    if rc:
        return module.fail_json(msg=err, rc=rc, cmd=cmd_str)
    # multiple lines can be returned if the chart is installed under different names and grep matches a substring
    # so we take the latest one i.e. very last one of the list
    out = out.splitlines()[-1:]

    if len(out) == 0 or not out[-1].strip():  # chart doesn't exist first time, install
        cmd_str = "%s install %s --name='%s' %s %s %s" % (command, 
            chart_namespace, chart_name, chart_location, helm_set_install_args, chart_values)
        (rc, out, err) = module.run_command(cmd_str, use_unsafe_shell=False)
        if rc:
            return module.fail_json(msg=err, rc=rc, cmd=cmd_str)
        result['changed'] = True
        result['message'] = 'Installed chart %s, version %s' % (chart_name, chart_version)
        result['original_message'] = out
        return module.exit_json(**result)
    elif out[-1].split()[0].lower() in ['deleted', 'failed']:
        cmd_str = "%s install %s --name='%s' --replace %s %s %s" % (command, 
            chart_namespace, chart_name, chart_location, helm_set_install_args, chart_values)
        (rc, out, err) = module.run_command(cmd_str, use_unsafe_shell=False)
        if rc:
            return module.fail_json(msg=err, rc=rc, cmd=cmd_str)
        result['changed'] = True
        result['message'] = 'Re-installed (previously deleted) chart %s, version %s' % (chart_name, chart_version)
        result['original_message'] = out
        return module.exit_json(**result)
    else:
        out = out[-1].split()  # we split lines and get an array back, now know it's non-empty so take last item
        deployment_status = out[0]
        deployed_version = parse_version(out[1].split('-')[-1])
        if deployment_status.lower() == "deployed" and chart_version > deployed_version:
            module.debug("Upgrading %s, deployed: %s, deploying: %s, current status: %s" % (
                chart_name, deployed_version, chart_version, deployment_status))
            cmd_str = "%s upgrade %s  %s %s %s %s" % (command, 
                chart_namespace, chart_name, chart_location, helm_set_upgrade_args, chart_values)
            (rc, out, err) = module.run_command(cmd_str, use_unsafe_shell=False)
            if rc:
                return module.fail_json(msg=err, rc=rc, cmd=cmd_str)
            result['changed'] = True
            result['message'] = 'Upgraded chart %s, version %s' % (chart_name, chart_version)
            result['original_message'] = out
            return module.exit_json(**result)
        elif chart_version < deployed_version:
            cmd_str = "%s history %s %s | grep '%s' | cut -f 1" % (command, 
                chart_namespace, chart_name, chart_version)
            (rc, out, err) = module.run_command(cmd_str, use_unsafe_shell=True)
            if rc:
                return module.fail_json(msg=err, rc=rc, cmd=cmd_str)
            if not out.strip():
                return module.fail_json(msg="Cannot downgrade %s has never been deployed at version %s. "
                                            "Its status is currently %s at a version of '%s'" % (
                                                chart_name, chart_version, deployment_status.lower(),
                                                str(deployed_version)))
            # Multiple revisions can have the same version
            # each one will be on their own line so take the newest i.e. the lat one
            deployed_revision = out.splitlines()[-1]
            cmd_str = "%s rollback %s %s %s" % (command, chart_namespace, chart_name, deployed_revision)
            # return module.fail_json(msg=[out,deployed_revision,cmd_str])
            (rc, out, err) = module.run_command(cmd_str, use_unsafe_shell=False)
            if rc:
                return module.fail_json(msg=err, rc=rc, cmd=cmd_str)
            result['changed'] = True
            result['message'] = 'Rolled back chart %s to version version %s, revision %s' % (
                chart_name, chart_version, deployed_revision)
            result['original_message'] = out
            return module.exit_json(**result)
    module.exit_json(**result)


def main():
    run_module()


if __name__ == '__main__':
    main()
